import React from 'react'

class Football extends React.Component {
    constructor(props){
        super(props);
        //this.shoot = this.shoot.bind(this);
        /*
        This binding is not required if shoot method is an arrow function.
        In arrow functions 'this' keyword always represents the object that defined
        the arrow funtion.
        */
    }

    shoot = (a, b) =>{
        alert(a);
    }

    render(){
        return(
            <div>
                <h1>Football</h1>
                <button
                    onClick={(ev) => this.shoot('Goal', ev)}
                    >Take the Shot!</button>
            </div>
        )
    }
}

export default Football;