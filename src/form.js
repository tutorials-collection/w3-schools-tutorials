import React from 'react'
import './index.css';

class InputForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            description: 'The content of a textarea goes in the value attribute.',
            username: '',
            age: null,
            errorMessage: '',
            myCar: 'Volvo'
        }
    }
    
    formChangeHandler = (event) =>{
        let name = event.target.name;
        let value = event.target.value;
        let error = '';

        if(name === 'age'){
            if(!Number(value)){
                error = <strong class="error">Age must be a number</strong>
            }
        }

        this.setState({
            [name]: value,
            errorMessage: error,
        });
    }

    formSubmitHandler = (event) =>{
        event.preventDefault();
        alert('You are submitting ' + this.state.username);
    }

    render(){
        let header;
        if(this.state.username){
            header = <div><p>Hello {this.state.username} {this.state.age}</p><br/></div>
        }else{
            header = '';
        }
        return(
            <form onSubmit={this.formSubmitHandler}>
                <h1>Input Form</h1>
                <textarea value={this.state.description}/>
                <br/>
                {header}
                <p>Name: </p>
                <input 
                    type='text'
                    name='username'
                    onChange={this.formChangeHandler}
                />
                <p>Age: </p>
                <input 
                    type='text'
                    name='age'
                    onChange={this.formChangeHandler}
                />
                <input
                    type='submit'
                />
                <br/>

                <select value={this.state.myCar}>
                    <option value='Ford'>Ford</option>
                    <option value='Volvo'>Volvo</option>
                    <option value='Fiat'>Fiat</option>
                </select>
                <p>{this.state.errorMessage}</p>
                
            </form>
        )
    }
}

export default InputForm;