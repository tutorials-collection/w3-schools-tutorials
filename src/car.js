import React from 'react';

class Car extends React.Component {
    constructor(props){
      super(props);
      this.state = {
          brand: props.info.brand,
          model: props.info.model,
          color: props.info.color,
          year: props.info.year,
      }
    }

    changeColor = () => {
        if(this.state.color === 'Red'){
            this.setState({
                color: 'Blue'
            })
        }else{
            this.setState({
                color: 'Red'
            })
        }        
    }

    render(){
      return (
          <div>
              <p>{this.state.color} {this.state.brand} {this.state.model} {this.state.year}</p>
              <button
                type='button'
                onClick={this.changeColor}
                >Change Color</button>
          </div>
      )
    }
}

export default Car;
