import React from 'react'

class LifeCycleDemo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            favoriteColor: 'Red',
            show: true,
        };
    }

    toggleChildVisibility = () =>{
        if(this.state.show){
            this.setState({
                show: false,
            });
        }else{
            this.setState({
                show: true,
            });
        }
        
    }

    componentDidMount(){
        setTimeout(
            () => {this.setState({favoriteColor: 'Yellow'})},
            1000
        );
    }

    getSnapshotBeforeUpdate(prevProps, prevState){
        document.getElementById('div1').innerHTML = 'Before update favorite: ' + prevState.favoriteColor;
    }

    componentDidUpdate(){
        document.getElementById('div2').innerHTML = 'After update favorite: ' + this.state.favoriteColor;
    }

    render() {
        let childComponent;
        let toggleButtonText;
        if (this.state.show) {
            childComponent = <Child />;
            toggleButtonText = 'Hide Child Component';
        }else{
            toggleButtonText = 'Show Child Component';
        }

        return (
          <div>
            <h1>Life Cycle Demo</h1>
            <p>My Favorite Color is {this.state.favoriteColor}</p>
            <br/>
            <p display id="div1"></p>
            <p id="div2"></p>
            <br/><br/>

            <p>Use the button below to show/hide the child component.</p>
            <br/>

            <button 
                type = 'button'
                onClick = {this.toggleChildVisibility}
            >{toggleButtonText}</button> 
            {childComponent}

          </div>
        );
      }
}

class Child extends React.Component{
    componentWillUnmount(){
        alert('The child component is about to be unmounted.');
    }

    render(){
        return(
            <div>
                <h1>Child Component</h1>
                <p>A component that 
                can be shown or hidden.</p>
            </div>

        );
    }
}

export default LifeCycleDemo;