import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Car from './car.js'
import LifeCycleDemo from './lifecycledemo.js'
import Football from './football';
import InputForm from './form'

class Garage extends React.Component {
  render() {
    const car1Info = {
      brand: 'Ford',
      model: 'Mustang',
      color: 'Red',
      year: 1964,
    };

    const car2Info = {
      brand: 'BMW',
      model: 'i8',
      color: 'Blue',
      year: 2020,
    };

    return (
      <div>
        <LifeCycleDemo/>
        <h1>Who lives in my Garage?</h1>
        <ul>
          <li><Car info={car1Info}/></li>
          <li><Car info={car2Info}/></li>
        </ul>
        <Football/>
        <br/>
        <InputForm/>
      </div>
    );
  }
}

ReactDOM.render(
  <Garage/>,
  document.getElementById('root')
);
